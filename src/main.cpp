#include <list>
#include <mutex>
#include <condition_variable>
#include "world.h"
#include "robot.h"

const int nRobots = 10;
std::mutex mtx;
std::promise<void> exitWorld;
std::future<void> wObj = exitWorld.get_future();

void loop(Robot &robot, World &w){
    mtx.lock();
    w.addRobot(robot.getId());
    mtx.unlock();

    while (wObj.wait_for(std::chrono::milliseconds(1)) == std::future_status::timeout){
        mtx.lock();
        w.moveRobot(robot.getId(), robot.move());
        mtx.unlock();
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
}

void loopWorld(World &w){
    while (wObj.wait_for(std::chrono::milliseconds(1)) == std::future_status::timeout) {
        w.display();
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
}

int main() {
    World w;
    std::thread wThread(loopWorld, std::ref(w));


    std::vector<std::thread> robotsT;
    Robot robots[nRobots];
    for(int i = 0; i < nRobots; ++i){
        robotsT.push_back(std::thread (loop, std::ref(robots[i]), std::ref(w)));
    }

    std::this_thread::sleep_for(std::chrono::seconds(10));
    exitWorld.set_value();

    for(std::thread & robot : robotsT){
        if(robot.joinable())
            robot.join();
    }
    wThread.join();

}

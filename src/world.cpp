#include "world.h"

World::World() {
    std::cout << "Creating world" << std::endl;
    srand(time(NULL));
    this->w = (rand() % (40 - 15 + 1)) + 15;
    this->h = (rand() % (25 - 15 + 1)) + 15;
    initMap();
    robotsid.clear();
    robotsPose.clear();
}

World::~World(){
   std::cout << "World is being deleted" << std::endl;
 }

void World::initMap(){

    map_.resize(h);
    for(int i = 0; i < h; i++){
        map_[i].resize(w);
    }
    for (int i = 0; i < (int) map_.size(); i++) {
        for (int j = 0; j < (int) map_[i].size() ; j++) {
            if (i == 0 || i == h - 1){
                map_[i][j] = '1';
            }

            else if(j==0 || j == w - 1){
                map_[i][j] = '1';
            }

            else{
            map_[i][j] = '-';
            }
        }
    }
    addObstacles();
}

void World::addObstacles(void){
    int numObstacles = (rand() % (30 - 10 + 1)) + 10;
    for(int i = 0; i < numObstacles; ++i){
        int x = (rand() % (w - 2 - 2 + 1)) + 2;
        int y = (rand() % (h - 2 - 2 + 1)) + 2;
        map_[y][x] = '1';
    }
}

bool World::addRobot(int id, int counter) {
    int x = (rand() % (w - 1 - 2 + 1)) + 2;
    int y = (rand() % (h - 1 - 2 + 1)) + 2;

    if(map_[y][x] == '-'){
        map_[y][x] = id;
        robotsid.push_back(id);
        robotsPose.push_back({x,y});
        return true;

    }
    else{
        ++counter;

        if(counter >= 5){
            std::cout<< "Robot no added: " << (char) id;
            return false;
        }
        addRobot(id, counter);
    }
    return true;
}

bool World::moveRobot(int id, int direction){

    std::vector<int>::iterator it;
    it = std::find(robotsid.begin(),robotsid.end(),id);
    int idx = it - robotsid.begin();

    switch(direction){

    case 0: //RIGHT
        if(map_[robotsPose[idx][1]][robotsPose[idx][0] + 1] != '-')
            return false;
        map_[robotsPose[idx][1]][robotsPose[idx][0]] = '-';
        ++robotsPose[idx][0];
        robotsPose[idx][1];
        break;

    case 1: //LEFT
        if(map_[robotsPose[idx][1]][robotsPose[idx][0] - 1] != '-')
            return false;
        map_[robotsPose[idx][1]][robotsPose[idx][0]] = '-';
        --robotsPose[idx][0];
        robotsPose[idx][1];
        break;

    case 2: //UP
        if(map_[robotsPose[idx][1] + 1][robotsPose[idx][0]] != '-')
            return false;
        map_[robotsPose[idx][1]][robotsPose[idx][0]] = '-';
        robotsPose[idx][0];
        ++robotsPose[idx][1];
        break;

    case 3: //DOWN
        if(map_[robotsPose[idx][1] - 1][robotsPose[idx][0]] != '-')
            return false;
        map_[robotsPose[idx][1]][robotsPose[idx][0]] = '-';
        robotsPose[idx][0];
        --robotsPose[idx][1];
        break;

    default:
        return false;
        break;

    }

    map_[robotsPose[idx][1]][robotsPose[idx][0]] = id;
    return true;
}


void World::display() {
  // "Clear" screen
    for (int i = 0; i < 20; i++)
        std::cout << std::endl;

    for(int i = 0; i != (int)map_.size(); i++){
        for(int j = 0; j!= (int)map_[i].size(); j++){
            std::cout << map_[i][j];
        }
        std::cout << std::endl;
    }
}

#ifndef WORLD_H_
#define WORLD_H_

#include <thread>
#include <future>
#include <vector>
#include <algorithm>
#include <iostream>
#include <chrono>
#include "robot.h"


// Forward declaration
class Robot;

// Define and implement the following class. Feel free to change the given
// interfaces and data structures to better match your design
class World {
 public:
    World();

    int w, h;
    void initMap();
    bool addRobot(int, int counter = 0);
    bool moveRobot(int id, int direction);
    void addObstacles(void);
    void display();
    std::vector<int> robotsid;
    std::vector<std::vector<int>> robotsPose;
    ~World();

 private:
    std::vector<std::vector<char>> map_;
};

#endif

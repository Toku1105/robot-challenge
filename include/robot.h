#ifndef ROBOT_H_
#define ROBOT_H_

#include "world.h"

// Forward declaration
class World;

// Define and implement the following class
class Robot {
 public:
  Robot();
  static int nRobots;
  int id_;
  int move();
  int getId();
  ~Robot();
};

#endif

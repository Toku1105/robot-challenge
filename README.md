# Robot Navigation Challenge Carbon Robotics

## DOCUMENTATION

"This program will simulate autonomous robots running around a 2D world. In addition to running without bugs and meeting all of the following design criteria, we will be evaluating the object-oriented design and coding style, so please code as if you were handing it off to a colleague to maintain. You do not need to write tests (unless it helps you), and full documentation is not necessary, but please briefly comment any non-obvious code.

This project has two classes for you to finish implementing, Robot and World. The World class encapsulates a simple 2D world.
* The world map is represented by a 2D data structure of your choice with each coordinate either blocked or unobstructed. The map is a rectangle of arbitrary size that is blocked around the entire border and has arbitrary internal coordinates blocked. You do not have to handle map configuration edge cases.
* World provides a way to place robots, and keeps track of the location of them. Each robot exists in a single coordinate space. There can be up to a few robots on the map at a time.
* World provides a movement API for the Robot class.
  * A robot can try to move a single space in any direction (holonomic), but if that space is blocked the robot does not move. Robot receives whether the move was successful or no
* World continuously displays the world (obstacles plus robots) to the terminal using ASCII. Below is example output with two robots A and B
```
1111111
1A00001
100B001
1001101
1001101
1000001
1111111
```

The Robot class defines a simple autonomous robot.
* The robot should move based on some simple behavior (such as move one space a second in one direction until blocked, then move in another direction). This behavior can be the same for each instance of Robot, and does not need to be complex.
* Robot cannot interact with or have knowledge about the world beyond the move API
* Each Robot instance should run independently and not depend on a global "tick" (ie, each should be in a different thread)

This project has a stub that builds with CMake and Make by running the following command from the project directory: mkdir build && cd build && cmake .. && make (we will be compiling it on gcc on Ubuntu). Take a look in main.cpp, robot.h, and world.h to see what has been implemented and for contextual instructions.





# Usage Instructions 



Clone the project
``` bash
git clone https://github.com/Toku11/robot-challenge.git

``` 
There is an executable inside the folder build, so the following commands should be used 

``` bash
cd /home/${USER}/robot-challenge/build && ./roboagents
``` 

<p align="center">
  <img src="contains/imagen3.png" width=676 height=450>
</p>

## Code Explain

The code follows the self-documented structure, so comments will not found inside the code. 
The World map initializes with a random size in each execution, as well as the position of the obstacles and the number of robots that will navigate in the environment.

## Compile 
if you need to compile the code please use the following commands 

``` bash
cd /home/${USER}/robot-challenge 
rm -rf build && mkdir build && cd build && cmake .. && make
./roboagents
``` 
<p align="center">
  <img src="contains/imagen2.png" width=676 height=450>
</p>





